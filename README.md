# Python-scraping

Selenium is an umbrella project for a range of tools and libraries that 
enable and support the automation of web browsers. Selenium brings together 
browser vendors, engineers, and enthusiasts to further an open discussion 
around automation of the web

### Directory Tree
```

├── .editorconfig
├── .gitignore
├── Makefile
├── README.md
├── requirements.txt
├── scraper.py
└── screenshots
    ├── screen_1.png
    ├── screen_2.png
    └── screen_3.png
```

### How to run the script

You can use the following command:

```
python3 scarper.py
```

If you have `make` command in you machine:

```
make scrap
```

> If it's first time, you would like delete the existing screenshots. You can 
> do it with `make delete_screenshots`
